#include <Ultrasonic.h>

#define SENS_TRIG_PIN (9) 
#define SENS_ECHO_PIN (8) 


char pinD2 = 12;
char pinD3 = 11;
char pinD4 = 10;
char pinD1 = 13;

Ultrasonic ultrasonic(SENS_TRIG_PIN, SENS_ECHO_PIN);

void setup() {
  pinMode(pinD1, OUTPUT);
  pinMode(pinD2, OUTPUT);
  pinMode(pinD3, OUTPUT);
  pinMode(pinD4, OUTPUT);   

  Serial.begin(9600);
}



void setPinsState( boolean vpinD1, boolean vpinD2, boolean vpinD3, boolean vpinD4)
{
    digitalWrite(pinD1, vpinD1);
    digitalWrite(pinD2, vpinD2);
    digitalWrite(pinD3, vpinD3);
    digitalWrite(pinD4, vpinD4); 
}


void setState(boolean deirection)
{
  static char state = 0;

  if(deirection)
  {
    switch(state)
    {
      case 0:
        setPinsState( HIGH, HIGH, LOW, LOW);
        state = 1;
      break;
      case 1:
        setPinsState( LOW, HIGH, HIGH, LOW);   
        state = 2;
      break;
      case 2:
        setPinsState( LOW, LOW, HIGH, HIGH);
        state = 3;
      break;
      default:
        setPinsState( HIGH, LOW, LOW, HIGH);
        state = 0;
      break;
    }
  }
  else
  {
    switch(state)
    {
      case 0:
        setPinsState( LOW, LOW, HIGH, HIGH);
        state = 1;
      break;
      case 1:
        setPinsState( LOW, HIGH, HIGH, LOW);  
        state = 2;
      break;
      case 2:
        setPinsState( HIGH, HIGH, LOW, LOW);
        state = 3;
      break;
      default:
        setPinsState( HIGH, LOW, LOW, HIGH);
        state = 0;
      break;
    }
  }
  delay(4);
}

uint16_t middle_of_3(uint16_t a, uint16_t b, uint16_t c)
{
   uint16_t middle;

   if ((a <= b) && (a <= c)){
      middle = (b <= c) ? b : c;
   }
   else{
      if ((b <= a) && (b <= c)){
         middle = (a <= c) ? a : c;
      }
      else{
         middle = (a <= b) ? a : b;
      }
   }

   return middle;
}

uint16_t array[3] = {0};
uint8_t cnt = 0;

void loop() {
  static char  dist_cm = 0;
  static char  lastdist_cm = 0;
  static char  diff = 0;

  
  array[cnt++] = ultrasonic.Ranging(CM);       // get distance  

  if (cnt == 3) {cnt = 0; }
  
  dist_cm = middle_of_3(array[0], array[1], array[2]);

  if (Serial.available())
  {
    Serial.println(dist_cm);
  }

  if ((dist_cm < 100) && (dist_cm > 5 ) && (lastdist_cm != dist_cm))
  {
    diff = lastdist_cm - dist_cm;

    diff *= 2;
    if (diff < 0)
    {
      diff = -diff;
      do {
        setState(false);
      } while(diff--);
    }
    else
    {
      do {
        setState(true);
      } while(diff--);
    }

    lastdist_cm = dist_cm;
  }
  //delay(100);
}










